<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/15/17
 * Time: 11:30 AM
 */
// Create connection


/*DEV MODE DEFINE*/
if (isset($_GET["devmode"]) && $_GET["devmode"] == "true") {

    define("DEVMODE", true);
}
else {

    define("DEVMODE", false);
}


$path = ROOT;

//if (strpos(ROOT,"tiengtrung") == false) $path = $path."/tiengtrung/";
//echo $path."/includes/Tiengtrung_conf.php";

require($path . "/includes/Tiengtrung_conf.php");
require($path . "/includes/api_config.php");

function connectdb($servername, $username, $password, $dbname)
{

    $conn = new mysqli($servername, $username, $password, $dbname);

    $conn->set_charset('utf8');

//$conn->query('SET NAMES UTF8');

    if ($conn->connect_error) {

        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}

$conn = connectdb($servername, $username, $password, $dbname);

?>