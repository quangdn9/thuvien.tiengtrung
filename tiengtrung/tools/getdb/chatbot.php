<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/15/17
 * Time: 11:28 AM
 */

require("getSimsimi.php");
require('../simsimi_api_class.php');

class Chatbot
{

//    private $cookie;

    private $chat;
    private $simsimi;
    private $type;

    /*DB*/
    private $conn;

    public function __construct($conn, $type = "cookie", $input)
    {
        $this->conn = $conn;

        if ($type == "api") {

            $this->simsimi = new SimSimiAPI($input); /*iNPUT IS API KEY*/

        } else if ($type == "cookie") {

//            $this->cookie = $input; /*COOKIE*/

            $this->simsimi = new getSimsimi($input); /*input is PARAM*/
        }

        $this->type = $type;

        return;

    }

    /*GET CHAT LIST*/
    public function searchByBot($text, $limit = 10)
    {

        $sql = "SELECT * FROM `simsimi` WHERE `simsimi`.`bot` LIKE '" . $text . "'  ORDER BY `simsimi`.`id` ASC  LIMIT 0, " . $limit;

        $result = $this->conn->query($sql);

        $data = [];

        if ($result->num_rows > 0) {

            while ($row = $result->fetch_object()) {

                $row->{"输入"} = $row->input;
                $row->{"机器"} = $row->bot;

                array_push($data, $row);

            }
        }

        return $data;
    }

    public function getChatList($limit = 10)
    {

        /*GET LAST ID*/
        $sql = "SELECT * FROM `chat_tbl1` chat
            WHERE chat.`输入` NOT IN (SELECT input FROM `simsimi` sim)
            GROUP BY chat.`输入` ORDER BY 1 ASC LIMIT 0," . $limit;

        $result = $this->conn->query($sql);

        $data = [];

        echo "<h1>Total rows: " . $result->num_rows . "</h1><br/>";

        if ($result->num_rows > 0) {

            while ($row = $result->fetch_object()) {
                array_push($data, $row);

            }
        }

        return $data;
    }

    public function updateReponse($id, $bot)
    {

        $sql = "UPDATE `simsimi` SET `bot` = '" . $bot . "' WHERE `id` = '" . $id . "'";

//        echo "<br/>";

        $this->conn->query($sql);

        return $this->conn->affected_rows;
    }

    /*INSERT SIMSIMI RESPONSE*/
    public function insertReponse($chat, $data)
    {

        /*$chat = $this->chat;
        $bot = $this->simsimi;*/
        $bot = "";

        /*COOKIE INPUT*/
        if ($this->type == "cookie") {

            $sim = json_decode($data["content"]);
            var_dump($sim);

            //INSERT DB
            if (isset($sim->respSentence)) $bot = $sim->respSentence;

        }
        /*API*/

        /*
         * 'response' => string '对的' (length=6)
          'id' => string '41582903' (length=8)
          'result' => int 100
          'msg' => string 'OK.' (length=3)
         * */

        /*API STYLE*/
        else
            $bot = $data;


        /*HAVE BOT RESPONSE DATA*/
        $sql = "INSERT INTO `simsimi` (`id`, `input`, `bot`, `time`) VALUES (NULL, '" . $chat . "', '" . $bot . "', CURRENT_TIMESTAMP)";

        $this->conn->query($sql);

        return $this->conn->insert_id;
    }

    /*GET CHAT RESPONSE*/
    public function getSimsimichat($chat)
    {

        $this->chat = $chat;
        $type = $this->type;

        if ($type == "api") {

            $data = $this->simsimi->simSimiConversation("zh", "1", "0", $chat);

        } else if ($type == "cookie") {

            $data = $this->simsimi->getChat($chat);
        }


        //var_dump($data);

        return $data;
    }

    /*GET BY API*/
}




