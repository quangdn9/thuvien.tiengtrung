var TiengTrung = {

		imageTitle: "",
		imageCategories: "",
		imageData: {},

		__construct: function() {

			/*TITLE*/
			var imageCategories = $("#Categories a:first").text();
			this.imageTitle = Image.title;

			this.imageCategories = imageCategories;
		},

		/*GET DETAIL*/
		getImageDetail: function() {

			$.get("getImageDetail.php", {

				title: this.imageTitle,
				cat: this.imageCategories

			}, function(data) {

				console.log(data);
				TiengTrung.init(data);

			});

			return;
		},

		init: function(data) {

			$("#theImage").append(

				$("<p>").html(data["内容"])
			);
		}


	};


$(function() {


	TiengTrung.__construct();
	//console.log(TiengTrung);
	TiengTrung.getImageDetail();
	
});